# I3 - The CSS Framework
I3 is a CSS3 framework inspired by Bootstrap. It uses CSS only for now. In newer versions like i4 you'll have to import jQuery.
# Credits
- GeoneveStudios
- DashStudios
###### GeoneveStudios® 2021 All Rights Reserved
